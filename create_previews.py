import shutil
import SimpleITK as sitk
import numpy as np
import core
import pandas
import argparse
from PIL import Image
from pathlib import Path

planeNames = ['axial', 'coronal', 'sagittal']

def saveImage(image, path, flipX=False, flipY=False):
    if len(image.shape) > 2:
        image[:,:,0] = 0
    pillowImage = Image.fromarray( image.astype(np.uint8) )
    
    if flipX:
        pillowImage = pillowImage.transpose(Image.FLIP_LEFT_RIGHT)
    if flipY:
        pillowImage = pillowImage.transpose(Image.FLIP_TOP_BOTTOM)
    # print('save image ', path, ' of size ', pillowImage.size)
    if Path(path).exists():
        existing_image = Image.open(path)
        eid = np.asarray(existing_image)
        pid = np.asarray(pillowImage)
        existing_image.close()
        if eid.shape != pid.shape or not np.all(np.equal(eid, pid)):
            raise Exception('Previews do not match nifti!')
        else:
            return path
    pillowImage.save(path, format='png')
    return path

def saveSlicesAt(imageToDisplay, path, n, boundingBoxSize, prefix=''):
    shape = imageToDisplay.shape
    center = [shape[0] // 2, shape[1] // 2, shape[2] // 2]
    halfBoundingBoxSize = boundingBoxSize//2
    image1 = imageToDisplay[max(0, min(center[0] + n, shape[0]-1)), :, :]
    image2 = imageToDisplay[:, max(0, min(center[1] + n, shape[1]-1)), :]
    image3 = imageToDisplay[:, :, max(0, min(center[2] + n, shape[2]-1))]
    flips = [(False, True), (False, True), (True, True)]
    paths = []
    for i, image in enumerate([image1, image2, image3]):
        imagePath = path / (prefix + planeNames[i] + '_' + str(n + halfBoundingBoxSize) + '.png')
        paths.append(imagePath)
        saveImage(image, str(imagePath), flips[i][0], flips[i][1])
    return paths

def saveSlices(imageToDisplay, path, sliceSpacing, boundingBoxSize):
    halfBoundingBoxSize = boundingBoxSize//2
    paths = []
    for n in range(-halfBoundingBoxSize, halfBoundingBoxSize+1, sliceSpacing):
        paths.append(saveSlicesAt(imageToDisplay, path, n, boundingBoxSize))
    paths.append(saveSlicesAt(imageToDisplay, path, 0, boundingBoxSize, 'center_'))
    return paths

def createMosaic(imagePaths, previewPath, mosaicSpacing):
    mosaicV = None
    mosaicH = None
    topV = 0
    leftH = 0

    for i in range(0, len(imagePaths), len(imagePaths) // mosaicSpacing):
        slicePaths = imagePaths[i]
        leftV = 0
        topH = 0
        for path in slicePaths:
            image = Image.open(str(path))
            if mosaicV is None or mosaicH is None:
                mosaicV = image
                mosaicH = image
            else:
                mosaicVWidth = mosaicV.size[0] + image.size[0] if topV == 0 else mosaicV.size[0]
                mosaicVHeight = mosaicV.size[1] + image.size[1] if leftV == 0 else mosaicV.size[1]
                newMosaicV = Image.new('L', (mosaicVWidth, mosaicVHeight))
                newMosaicV.paste(mosaicV, (0, 0, mosaicV.size[0], mosaicV.size[1]))
                box = (leftV, topV, leftV + image.size[0], topV + image.size[1])
                newMosaicV.paste(image, box)
                mosaicV = newMosaicV

                mosaicHWidth = mosaicH.size[0] + image.size[0] if topH == 0 else max(mosaicH.size[0], leftH + image.size[0])
                mosaicHHeight = mosaicH.size[1] + image.size[1] if leftH == 0 else mosaicH.size[1]
                newMosaicH = Image.new('L', (mosaicHWidth, mosaicHHeight))
                newMosaicH.paste(mosaicH, (0, 0, mosaicH.size[0], mosaicH.size[1]))
                box = (leftH, topH, leftH + image.size[0], topH + image.size[1])
                newMosaicH.paste(image, box)
                mosaicH = newMosaicH

            leftV += image.size[0]
            topH += image.size[1]
        if mosaicV is None or mosaicH is None: continue
        topV = mosaicV.size[1]
        leftH = mosaicH.size[0]
    if mosaicV is None or mosaicH is None: return
    mosaicV.save(str(previewPath / 'mosaicV.png'), format='png')
    mosaicH.save(str(previewPath / 'mosaicH.png'), format='png')
    return

def createPreviewPathLink(previewPathLink, previewPath):
    if not previewPathLink.exists(): 
        previewPathLink.parent.mkdir(exist_ok=True, parents=True)
        previewPathLink.symlink_to(previewPath, target_is_directory=True)
    return

def resample_image(nifti, newSpacing):
    resampler = sitk.ResampleImageFilter()
    size = nifti.GetSize()
    spacing = nifti.GetSpacing()
    resampler.SetSize([ int(size[0] * spacing[0] / newSpacing[0]), int(size[1] * spacing[1] / newSpacing[1]), int(size[2] * spacing[2] / newSpacing[2]) ])
    resampler.SetOutputSpacing(newSpacing)
    resampler.SetOutputOrigin(nifti.GetOrigin())
    resampler.SetOutputDirection(nifti.GetDirection())
    resampler.SetInterpolator(sitk.sitkLinear)
    return resampler.Execute(nifti)

def create_preview(niftiPath, previewPath, newSpacing=(0.5, 0.5, 0.5), sliceSpacing=5, boundingBoxSize=200, mosaicSpacing=5, overwrite=False):
    if previewPath.exists() and overwrite:
        shutil.rmtree(previewPath)

    previewPath.mkdir(exist_ok=True, parents=True)
    
    reader = sitk.ImageFileReader()
    reader.SetFileName(str(niftiPath))
    reader.ReadImageInformation()
    sitk.GetPixelIDValueAsString(reader.GetPixelID())
    
    if len(reader.GetSize()) != 3:
        raise core.download.DownloadException('nifti_not_3D', f'Cannot process an image with {len(reader.GetSize())} dimensions (must be 3D).')
    
    if np.any([s < 5 for s in reader.GetSize()]):
        raise core.download.DownloadException('nifti_not_3D', f'Cannot process an image with dimensions {reader.GetSize()} (all dimensions must be greater than 5).')

    if reader.GetNumberOfComponents() > 1:
        raise core.download.DownloadException('nifti_not_FLAIR', f'Cannot process an image with {reader.GetNumberOfComponents()} components.')
    
    nifti = sitk.ReadImage(str(niftiPath))
    
    # if 'vector' in nifti.GetPixelIDTypeAsString():
    #     raise core.download.DownloadException('nifti_not_FLAIR', f'Cannot process an image of type {nifti.GetPixelIDTypeAsString()}.')

    result = resample_image(nifti, newSpacing)

    niftiData = sitk.GetArrayFromImage(result)
    minValue = np.min(niftiData)
    maxValue = np.max(niftiData)
    if maxValue > minValue:
        niftiData = (255.0 * (niftiData - minValue)) / float(maxValue - minValue)
    
    imagePaths = saveSlices(niftiData, previewPath, sliceSpacing, boundingBoxSize)
    return createMosaic(imagePaths, previewPath, mosaicSpacing)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog=__file__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""Create nifti previews""")

    parser.add_argument('-d', '--data', required=True, help='Path to the directory containing the niftis.')
    parser.add_argument('-o', '--output', default=None, help='Path to the destination directory. Defaults to the same directory as the input directory.')
    parser.add_argument('-s', '--slice_spacing', default=5, help='The number of slices per preview image (default = 5, meaning it will save 1 slice every 5 slices).')
    parser.add_argument('-n', '--bounding_box_size', default=200, help='Number of frames to consider for the preview bounding box (images are resampled on 0.5x0.5x0.5 mm).')
    parser.add_argument('-m', '--mosaic_spacing', default=5, help='Number of preview images in mosaic.')
    parser.add_argument('-c', '--check_existing_previews_match_nifti', action='store_true', help='Check if existing preview images match the nifti.')
    parser.add_argument('-p', '--paths', help='Only generate previews from given paths (csv with a path column).')


    args = parser.parse_args()

    niftis = Path(args.data)
    destinationDirectory = Path(args.output) if args.output else None
    if destinationDirectory:
        destinationDirectory.mkdir(parents=True, exist_ok=True)
    sliceSpacing = int(args.slice_spacing)
    boundingBoxSize = int(args.bounding_box_size)
    mosaicSpacing = int(args.mosaic_spacing) - 1

    newSpacing = (0.5, 0.5, 0.5)

    mismatch_records = []
    paths_to_process = pandas.read_csv(args.paths) if args.paths else None
    
    for patientPath in sorted(list(niftis.iterdir())):
        
        if not patientPath.is_dir(): continue
        
        times = sorted(list(patientPath.iterdir()))

        for time in times:
            
            niftiPaths = sorted(list(time.glob('**/*.nii.gz')))

            for niftiPath in niftiPaths:
                
                if paths_to_process is not None and str(niftiPath) not in list(paths_to_process.path): continue

                print(niftiPath)

                previewPathLink = time / 'preview'
                previewPath = destinationDirectory / patientPath.name / time.name / 'preview' if destinationDirectory else previewPathLink

                if (not args.check_existing_previews_match_nifti) and previewPath.exists():
                    createPreviewPathLink(previewPathLink, previewPath)
                    continue
                
                previewPath.mkdir(exist_ok=True, parents=True)
                createPreviewPathLink(previewPathLink, previewPath)

                if (not args.check_existing_previews_match_nifti) and (previewPath / 'mosaicV.png').exists() and (previewPath / 'mosaicH.png').exists(): continue

                try:        
                    create_preview(niftiPath, previewPath, newSpacing, sliceSpacing, boundingBoxSize, mosaicSpacing)
                except Exception as e:
                    if args.check_existing_previews_match_nifti:
                        mismatch_records.append({ 'patient': patientPath.name, 'time': time.name, 'name': f'{patientPath.name}_{time.name}', 'error': str(e), 'path': str(niftiPath) })
                        pass
                    continue

    if args.check_existing_previews_match_nifti:
        mismatches = pandas.DataFrame.from_records(mismatch_records)
        mismatches.to_csv('datasheets/recovery/preview_mismatches.csv', index=False)