# Quality Control

The `process.py` script enable to download datasets from Shanoir (in DICOM), convert them (to Nifti) & generate previews (set of .png images).
It takes a list of datasets (a .csv file) as input, the file must have the following columns:

`sequence_id,id_hd,shanoir_name,series_description,acquisition_date,DATE_V0`

and the script will output a new csv file (based from the given one) with the following additional columns:

`previously_sent,artifactType,valid,comment,downloaded,converted,reconverted,conversion_tool,ready_for_quality_control`

Example: `python process.py -d reference.csv -u amasson -hdr /data/hd_cohort/dicoms/`

The `qc_gui.py` script launches a GUI to preview and validate the datasets.

Example: `python qc_gui.py -s reference.csv -d /data/hd_cohort/dicoms/`