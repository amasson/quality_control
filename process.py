import subprocess
import datetime
import re
import logging
import shutil
import tempfile
import pandas
from pathlib import Path
import sys
sys.path.append('/home/amasson/shanoir_downloader')
import shanoir_downloader, convert_dicoms_to_niftis
import core.download
import create_previews

# Debug segmentation fault
import faulthandler
faulthandler.enable()

parser = shanoir_downloader.create_arg_parser('Process HD Cohort')

parser.add_argument('-d', '--datasets', required=True, help='The excel or csv file containing the datasets to download. This file will be updated with process information, save as [--datasets]_reference_[datetime].csv, and can be reused as a reference during the next processing (see --reference argument).')
parser.add_argument('-r', '--reference', help='The csv file containing all information about the datasets considered until now. The datasets to download will be added to this list and saved to the [--datasets]_reference_[datetime].csv file, so it can be reused as a reference during the next processing. By default, the datasets to download which already exist in --reference will be ignored (assuming they are already downloaded since in the reference file). The option --overwrite_datasets enables to reprocess existing datasets. If it is not provided, the file --datasets will be used (and updated).')
parser.add_argument('-od', '--overwrite_datasets', action='store_true', help='Reprocess existing datasets (overwrite datasets which are both in the --datasets and --reference files).')
parser.add_argument('-hd', '--hd_folder', help='The hd cohort folder (optional, use either hd_folder or remote and hd_on_remote).', default=None)
parser.add_argument('-rmt', '--remote', help='The remote storage address.', default=None)
parser.add_argument('-hdr', '--hd_on_remote', help='The hd cohort folder on remote storage.', default=None)
shanoir_downloader.add_username_argument(parser)
parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Print log messages.')
parser.add_argument('-t', '--timeout', type=float, default=60*4, help='The request timeout.')
parser.add_argument('-lf', '--log_file', type=str, help="Path to the log file. Default is current_working_directory/downloads.log", default=None)
args = parser.parse_args()

if args.hd_folder is None and (args.remote is None or args.hd_on_remote is None):
	sys.exit('Error: either hd_folder or remote and hd_on_remote arguments must be set.')

dicoms_folder = Path(args.hd_folder) / 'dicoms' if args.hd_folder is not None else None
if dicoms_folder is not None:
	dicoms_folder.mkdir(exist_ok=True, parents=True)

setattr(args, 'output_folder', Path.cwd())
setattr(args, 'domain', 'shanoir-ofsep.irisa.fr')

config = shanoir_downloader.initialize(args)

datasets_dtype = {'sequence_id': str, 'shanoir_name': str, 'series_description': str, 'patient_name_in_dicom': str, 'series_description_in_dicom': str}

def ignore_currently_out(datasets, name):
	if 'currently_out' in datasets.columns and len(datasets[datasets.currently_out != 1]) > 0:
		print(f'Ignoring all {name} with currently_out == 1')
	return datasets[datasets.currently_out != 1] if 'currently_out' in datasets.columns else datasets

def check_duplicates(datasets, name):
	if datasets.sequence_id.duplicated().sum() > 0:
		print(f'Warning: there are duplicated {name}:')
		print(datasets[datasets.sequence_id.duplicated(keep=False)])
		resume = input('Do you want to continue? y/n\n')
		if 'n' in resume.lower(): sys.exit()
	return

# Warning: remove the currently out datasets to avoid duplicated sequence_id! The assumption is that currently_out datasets should not be processed
datasets = pandas.read_excel(args.datasets, dtype=datasets_dtype) if args.datasets.endswith('xlsx') else pandas.read_csv(args.datasets, sep='\t' if args.datasets.endswith('tsv') else ',', dtype=datasets_dtype)
if pandas.api.types.is_datetime64_any_dtype(datasets.acquisition_date):
	datasets.acquisition_date = datasets.acquisition_date.dt.strftime('%Y-%m-%d')

datasets = ignore_currently_out(datasets , 'datasets to download')
check_duplicates(datasets, 'datasets to download')

if args.reference:
	reference = pandas.read_csv(args.reference, sep='\t' if args.reference.endswith('tsv') else ',', dtype=datasets_dtype)
	reference = ignore_currently_out(reference, 'datasets in the reference file')
	check_duplicates(reference, 'datasets in the reference file')
	
	reference = pandas.concat([reference, datasets[~datasets.sequence_id.isin(reference.sequence_id)]])
	
	reference = reference.set_index('sequence_id')
	datasets = datasets.set_index('sequence_id')
	comparison = reference.loc[reference.index.intersection(datasets.index)][datasets.columns].sort_index().compare(datasets.sort_index(), result_names=("reference", "datasets_to_download"))
	if len(comparison) > 0:
		print('Some datasets are different in the reference and in the datasets to download files:')
		print(comparison)
		if args.overwrite_datasets:
			print('The reference datasets will be replaced by the datasets to download since you set the --overwrite_datasets option. For those datasets, the "valid" column will be reset to None.')
		resume = input('Do you want to continue? y/n\n')
		if 'n' in resume.lower(): sys.exit()
	if args.overwrite_datasets:
		reference.update(datasets.fillna('OverwriteNaN'))
		reference.replace('OverwriteNaN', None, inplace=True)
		reference.loc[datasets.index, 'valid'] = None
		reference.loc[datasets.index, 'ready_for_quality_control'] = None
	datasets = reference.reset_index()
else:
	n_already_processed = datasets.ready_for_quality_control.notna().sum() if 'ready_for_quality_control' in datasets.columns else 0
	if n_already_processed > 0:
		logging.warning(f'{n_already_processed} dataset(s) already processed ("ready_for_quality_control" is defined), will be ignored.')

for column in ['downloaded', 'converted', 'reconverted', 'conversion_tool', 'ready_for_quality_control']:	
	if not column in datasets.columns:
		datasets[column] = None

def update_datasets(datasets, index, datasets_path, type, message):
	logging.warning(f'Error {type} while processing dataset {dataset_name} ({dataset_id}): {message}')
	datasets.at[index, 'ready_for_quality_control'] = False
	datasets.at[index, 'download_error_type'] = type
	datasets.at[index, 'download_error_message'] = message
	datasets.to_csv(datasets_path, index=False)
	return

def check_dicom_shanoir_match(datasets, index, datasets_path, dataset, dicom):
	shanoir_name = str(dataset['shanoir_name']) if 'shanoir_name' in dataset else None
	series_description = str(dataset['series_description']) if 'series_description' in dataset else None

	# A name or description mismatch is not enough to cancel the processing, just update_datasets and continue processing instead of throwing an exception
	if shanoir_name is not None and shanoir_name != str(dicom.PatientName):
		update_datasets(datasets, index, datasets_path, 'dicom_shanoir_name_mismatch', f'Shanoir name {shanoir_name} differs in dicom: {dicom.PatientName}')
		# raise core.download.DownloadException('dicom_shanoir_name_mismatch', f'Shanoir name {shanoir_name} differs in dicom: {dicom.PatientName}')
	
	if series_description is not None and (not hasattr(dicom, 'SeriesDescription') or series_description.replace(' ', '') != str(dicom.SeriesDescription).replace(' ', '')):
		update_datasets(datasets, index, datasets_path, 'dicom_shanoir_description_mismatch', f'Shanoir description {series_description} differs in dicom: {dicom.SeriesDescription}')
		# raise core.download.DownloadException('dicom_shanoir_description_mismatch', f'Shanoir description {series_description} differs in dicom: {dicom.SeriesDescription}')
	
	return

# Create a new reference from merged datasets (the given reference and the datasets to download) file with the current date
datasets_path = Path(args.datasets)
match = re.match("(.*)reference_[0-9_hm-]+", datasets_path.stem)
datasets_name = f'{match.group(1)}' if match else '' # datasets_path.stem
datasets_path = datasets_path.parent / f'{datasets_name}reference_{datetime.datetime.now().strftime("%Y-%m-%d_%Hh%Mm%S")}.csv'

def dataset_exists(dicoms_folder, dataset_name, dataset_id):
	return ( (dicoms_folder / dataset_name / f'preview_{dataset_id}').exists()
	 and (dicoms_folder / dataset_name / f'{dataset_id}.nii.gz').exists()
	 and len(list((dicoms_folder / dataset_name / f'preview_{dataset_id}').glob('*.png'))) > 0 )

def dataset_exists_on_remote(remote, dicoms_folder, dataset_name, dataset_id):
	dataset_folder = Path(dicoms_folder) / dataset_name
	nifti = dataset_folder / f'{dataset_id}.nii.gz'
	preview_folder = dataset_folder / f'preview_{dataset_id}'
	output = subprocess.call(['ssh', remote, f'[ -f {nifti} -a -d {preview_folder} ]'])
	return output == 0

skip_existing_niftis = None
# overwrite_niftis = None
# The ready_for_quality_control column is not None when the dataset has been handled (dowloaded, converted, ready_for_quality_control) either because it was successfully handled or not.
for index, dataset in datasets[datasets.ready_for_quality_control.isna()].iterrows():
	
	dataset_id = dataset['sequence_id']
	dataset_name = dataset['id_hd'] + '_' + dataset['acquisition_date']

	with tempfile.TemporaryDirectory() as temporary_directory:
		
		destination_folder = Path(temporary_directory)
		downloaded_archive = destination_folder / 'downloaded_archive'

		if dataset_exists_on_remote(args.remote, args.hd_on_remote, dataset_name, dataset_id) if args.remote is not None else dataset_exists(dicoms_folder, dataset_name, dataset_id):
			skip_existing_niftis = skip_existing_niftis or input(f'Dataset {dataset_name} is already processed. Do you want to overwrite existing datasets or skip them? Enter "o" to overwrite or any other key to skip them.\n')
			if "o" not in skip_existing_niftis: continue

		try:
			logging.info(f'Download dataset {dataset_name} ({dataset_id})')
			
			datasets.at[index, 'downloaded'] = False
			dicom_folder, dicom = core.download.download_dataset(config, dataset_id, downloaded_archive)
			datasets.at[index, 'downloaded'] = True
			datasets.to_csv(datasets_path, index=False)
			
			logging.info(f'Check Shanoir name and description in Dicom')
			
			check_dicom_shanoir_match(datasets, index, datasets_path, dataset, dicom)
			
			logging.info(f'Convert dicom to nifti')
			
			# if a nifti already exists because of a previous download: ask if erase or skip
			nifti_paths = list(destination_folder.glob('**/*.nii.gz'))
			if len(nifti_paths) > 0:
				# overwrite_niftis = overwrite_niftis or input('Some niftis already exists. Do you want to overwrite them and continue? This will apply for all datasets. y/n.\n')
				# if 'n' in overwrite_niftis.lower(): sys.exit()
				for nifti in nifti_paths: nifti.unlink()
			conversion_tool, reconverted, image_converted = convert_dicoms_to_niftis.convert_dicom_to_nifti(dicom_folder, destination_folder)
			datasets.at[index, 'converted'] = image_converted is not None
			datasets.at[index, 'reconverted'] = reconverted
			datasets.at[index, 'conversion_tool'] = conversion_tool
			
			datasets.to_csv(datasets_path, index=False)

			shutil.rmtree(downloaded_archive)
			shutil.rmtree(dicom_folder)
			
			if image_converted is None:
				raise core.download.DownloadException('conversion_error', 'Unable to convert the image.')

			nifti_paths = list(destination_folder.glob('**/*.nii.gz'))
			if len(nifti_paths) == 0:
				raise core.download.DownloadException('conversion_error', 'Unable to find the nifti file.')
			if len(nifti_paths) > 1:
				raise core.download.DownloadException('conversion_error', 'More than one nifti file.')

			logging.info(f'Create preview')
			nifti_path = nifti_paths[0]
			nifti_path = nifti_path.rename(nifti_path.parent / f'{dataset_id}.nii.gz')
			create_previews.create_preview(nifti_path, destination_folder / f'preview_{dataset_id}', overwrite=True)

			# Rsync will just move the data if on same device
			logging.info(f'Transfer dataset')
			try:
				subprocess.run(['rsync', '-avz', f'{destination_folder}/', f'{args.remote}:{args.hd_on_remote}/{dataset_name}' if args.remote is not None else dicoms_folder / dataset_name], check=True, stdout=subprocess.DEVNULL)
			except subprocess.CalledProcessError as e:
				raise core.download.DownloadException('transfer_error', str(e))
			
			datasets.at[index, 'ready_for_quality_control'] = True
			datasets.to_csv(datasets_path, index=False)
			
		except core.download.DownloadException as e:
			update_datasets(datasets, index, datasets_path, e.error_type, e.error_message)
			pass
		except Exception as e:
			update_datasets(datasets, index, datasets_path, 'unknown', str(e))
			pass
