from datetime import datetime
import time
import zipfile
import pydicom
import requests
import shanoir_downloader

class DownloadException(Exception):
	def __init__(self, error_type, error_message):
		super().__init__(error_type, error_message)
		self.error_type = error_type
		self.error_message = error_message

SHANOIR_SHUTDOWN_HOUR = 2
SHANOIR_AVAILABLE_HOUR = 5

def download_dataset(config, dataset_id, destination_folder):
	destination_folder.mkdir(exist_ok=True, parents=True)
	config['output_folder'] = destination_folder
	
	# Shanoir server reboot every nigth ; between 3 and 4 AM: sleep until shanoir server is awake 
	now = datetime.now()
	if now.hour >= SHANOIR_SHUTDOWN_HOUR and now.hour < SHANOIR_AVAILABLE_HOUR:
		future = datetime(now.year, now.month, now.day, SHANOIR_AVAILABLE_HOUR, 0)
		time.sleep((future-now).total_seconds())
	
	# Download the dataset
	try:
		shanoir_downloader.download_dataset(config, dataset_id, 'dicom', True)
	except requests.HTTPError as e:
		message = f'Response status code: {e.response.status_code}, reason: {e.response.reason}'
		if hasattr(e.response, 'error') and e.response.error:
			message += f', response error: {e.response.error}'
		message += str(e)
		raise DownloadException('status_code_' + str(e.response.status_code), message)
	except Exception as e:
		raise DownloadException('unknown_download_error', str(e)) from e
	
	# List the downloaded zip files
	zip_files = list(destination_folder.glob('*.zip'))

	if len(zip_files) != 1:
		message = f'No zip file was found' if len(zip_files) == 0 else f'{len(zip_files)} zip files were found'
		message += f' in the output directory {destination_folder}.'
		message += f' Downloaded files: { destination_folder.ls() }'
		raise DownloadException('zip', message)

	# Extract the zip file
	dicom_zip = zip_files[0]
	
	dicom_folder = destination_folder.parent / f'{dataset_id}'
	dicom_folder.mkdir(exist_ok=True)
	
	with zipfile.ZipFile(str(dicom_zip), 'r') as zip_ref:
		zip_ref.extractall(str(dicom_folder))
		
	dicom_files = list(dicom_folder.glob('*.dcm'))

	# Error if there are no dicom file found
	if len(dicom_files) == 0:
		raise DownloadException('nodicom', f'No DICOM file was found in the dicom directory {dicom_folder}.')
	
	dicom_file = dicom_files[0]
	ds = pydicom.dcmread(str(dicom_file))
	with open(destination_folder.parent / 'dicom.json', 'w') as f:
		try:
			f.write(ds.to_json())
		except Exception as e:
			pass # sometimes pydicom cannot convert the dicom to json, ignore those cases
		
	return dicom_folder, ds