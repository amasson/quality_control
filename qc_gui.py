import pandas
import numpy as np
import collections
import argparse
from pathlib import Path
import shutil
import sys
import os
import json
# Path.ls = lambda x: sorted(list(x.iterdir()))
import re
import subprocess
from PySide2 import QtGui, QtCore, QtWidgets
from PySide2.QtCore import Qt, QAbstractTableModel, QAbstractItemModel, QModelIndex
from PySide2.QtGui import QColor, QFont
from PySide2.QtCore import QObject, Signal, Slot, QTimer
from functools import partial
import re
from natsort import index_natsorted


parser = argparse.ArgumentParser(
	prog=__file__,
	formatter_class=argparse.RawDescriptionHelpFormatter,
	description="""Quality Control GUI""")

parser.add_argument('-d', '--data', help='Path to the niftis.', required=True)
parser.add_argument('-s', '--datasets', help='Path to the reference dataset list.', required=True)
parser.add_argument('-p', '--preview', default=None, help='Path to the preview files. Defaults to the data path containing the niftis.')

args = parser.parse_args()
datasets = pandas.read_excel(args.datasets, index_col='sequence_id') if args.datasets.endswith('.xlsx') or args.datasets.endswith('.xls') else pandas.read_csv(args.datasets, index_col='sequence_id')

planeNames = ['axial', 'sagittal', 'coronal']

def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

animationSpeed = 4

class QCGUI(QtWidgets.QWidget):

	def warning_modal(self, title, message, buttons=QtWidgets.QMessageBox.Abort | QtWidgets.QMessageBox.Ok):
		modal = QtWidgets.QMessageBox(self)
		modal.setStandardButtons(buttons)
		modal.setWindowTitle(title)
		modal.setText(message)
		button = modal.exec_()
		if button == QtWidgets.QMessageBox.Abort:
			sys.exit(-1)
		return
	
	def get_nifti_stem(self, nifti):
		return nifti.name[:-len('.nii.gz')]

	def __init__(self, datasets, data_path, preview_path, datasets_path):
		super().__init__()

		self.datasets_path = datasets_path
		self.dataDirectory = Path(data_path)

		self.previewDirectory = Path(preview_path) if preview_path is not None else self.dataDirectory
		
		self.datasets = datasets.sort_values(by="id_hd", key=lambda x: np.argsort(index_natsorted(datasets["id_hd"])))
		self.patients = list(self.datasets[(self.datasets.ready_for_quality_control == True) & self.datasets.valid.isna()].id_hd.unique())

		if len(self.patients) == 0:
			self.warning_modal("Warning: no datasets to control.", "There are no datasets to control (both ready_for_quality_control and valid) in the given reference spreadsheet.", QtWidgets.QMessageBox.Abort)
			return
		
		patientDirs = [p for p in list(self.previewDirectory.iterdir()) if p.is_dir()]
		
		# Warn if some flairs have no previews
		flairsInPatients = [flair for p in patientDirs for flair in p.glob('*.nii.gz')]
		flairsWithoutPreviews = [f'{p.parent.name}/{p.name}' for p in flairsInPatients if not (p.parent / f'preview_{self.get_nifti_stem(p)}').exists()]
		if len(flairsWithoutPreviews) > 0:
			self.warning_modal("Warning: flairs without previews", f"The following flairs do not have previews: {flairsWithoutPreviews}.")

		# Warn if some patient directories are not in datasets
		datasetsInDirs = [ flair for p in patientDirs for flair in p.glob('*.nii.gz')]
		patientsNotInDatasets = [f'{p.parent.name}/{p.name}' for p in datasetsInDirs if len(self.datasets[ (self.datasets.id_hd == p.parent.name.split('_')[0]) & (self.datasets.acquisition_date == p.parent.name.split('_')[1]) & ( self.datasets.index == int(self.get_nifti_stem(p)) ) ])==0]
		if len(patientsNotInDatasets) > 0:
			self.warning_modal("Warning: patients absent in datasets", f"The following patients are not in the datasets spreadsheet: {patientsNotInDatasets}.")

		# Warn if some datasets are ready_for_quality_control and valid == None but do not have corresponding dirs
		sds = self.datasets[(self.datasets.ready_for_quality_control == True) & self.datasets.valid.isna()]
		patientsWithoutDir = [f'{r.id_hd}_{r.acquisition_date}/{i}.nii.gz' for i, r in sds.iterrows() if not (self.previewDirectory / f'{r.id_hd}_{r.acquisition_date}/{i}.nii.gz').exists() ]
		if len(patientsWithoutDir) > 0:
			self.warning_modal("Warning: patients without directories", f"The following patients have no directories in the preview directory, but are set ready_for_quality_control (and valid is None): {patientsWithoutDir}.")

		self.imageWidget = None
		self.imageWidgetLayout = None
		
		self.currentPatient = self.patients[0]
		self.currentImage = 0
		self.imagePaths = {}

		# self.inOutPathsLayout = QtWidgets.QHBoxLayout()
		# patientDirLayout, _, _, _ = self.createFileUI("Patients dir", self.onPatientDirChange)
		# self.inOutPathsLayout.addLayout(patientDirLayout)
		# previewDirLayout, _, self.previewDirLineEdit, _ = self.createFileUI("Preview dir", self.onPreviewDirChange)
		# self.inOutPathsLayout.addLayout(previewDirLayout)
		
		self.viewModes = ["All", "Axial", "Vertical mosaic", "Horizontal mosaic"]
		self.viewModeComboBox = QtWidgets.QComboBox()
		self.viewModeComboBox.addItems(self.viewModes)
		self.viewModeComboBox.setCurrentIndex(1)
		self.viewModeComboBox.currentIndexChanged.connect(self.viewModeChange)

		self.previousUnseenButton = QtWidgets.QPushButton("Previous unseen")
		self.previousUnseenButton.clicked.connect(self.previousUnseenPatient)
		self.previousButton = QtWidgets.QPushButton("Previous")
		self.previousButton.clicked.connect(self.previousPatient)
		self.nextButton = QtWidgets.QPushButton("Next")
		self.nextButton.clicked.connect(self.nextPatient)
		self.nextUnseenButton = QtWidgets.QPushButton("Next unseen")
		self.nextUnseenButton.clicked.connect(self.nextUnseenPatient)

		self.openButton = QtWidgets.QPushButton("Open")
		self.openButton.clicked.connect(self.openImageInFSLEyes)
		
		self.vLayout = QtWidgets.QVBoxLayout()
		
		# self.vLayout.addLayout(self.inOutPathsLayout)

		self.vLayout.addWidget(self.viewModeComboBox)

		self.vLayout.addWidget(self.createImageWidget())

		self.speedHLayout = QtWidgets.QHBoxLayout()
		self.speedLabel = QtWidgets.QLabel(self)
		self.speedLabel.setText('Animation Speed:')
		self.speedHLayout.addWidget(self.speedLabel)
		self.speedSpinBox = QtWidgets.QSpinBox(self)
		self.speedSpinBox.setMinimum(0)
		self.speedSpinBox.setMaximum(5)
		self.speedSpinBox.setValue(animationSpeed)
		self.speedSpinBox.setSingleStep(1)
		self.speedSpinBox.valueChanged.connect(self.speedChanged)
		self.speedHLayout.addWidget(self.speedSpinBox)

		self.vLayout.addLayout(self.speedHLayout)
		self.patientLabel = QtWidgets.QLabel(self)
		self.patientLabel.setText('Patient:')
		self.patientHLayout = QtWidgets.QHBoxLayout()
		self.patientHLayout.addWidget(self.patientLabel)
		self.patientLineEdit = QtWidgets.QLineEdit(self)
		self.patientLineEdit.setText(self.currentPatient)
		self.patientLineEdit.editingFinished.connect(self.setCurrentPatient)
		self.patientHLayout.addWidget(self.patientLineEdit)
		self.vLayout.addLayout(self.patientHLayout)

		# self.checkbox1 = QtWidgets.QCheckBox("Temporal point 1 valid")
		# self.checkbox1.setCheckState(Qt.Checked)
		# self.checkbox1.stateChanged.connect(partial(self.checkboxChanged, timePoint='01'))
		# self.checkbox2 = QtWidgets.QCheckBox("Temporal point 2 valid")
		# self.checkbox2.setCheckState(Qt.Checked)
		# self.checkbox2.stateChanged.connect(partial(self.checkboxChanged, timePoint='02'))
		
		self.vTimePointWidget = QtWidgets.QWidget()
		self.vTimePointScrollArea = QtWidgets.QScrollArea()
		self.vTimePointLayout = QtWidgets.QVBoxLayout()
		
		self.vTimePointScrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
		self.vTimePointScrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		self.vTimePointScrollArea.setWidgetResizable(True)
		self.vTimePointScrollArea.setWidget(self.vTimePointWidget)
		self.vTimePointWidget.setLayout(self.vTimePointLayout)
		
		self.checkboxes = {}
		
		self.hLayout2 = QtWidgets.QHBoxLayout()
		self.hLayout2.addWidget(self.previousUnseenButton)
		self.hLayout2.addWidget(self.previousButton)
		self.hLayout2.addWidget(self.nextButton)
		self.hLayout2.addWidget(self.nextUnseenButton)
		self.vLayout.addWidget(self.openButton)
		# self.vLayout.addLayout(self.vTimePointLayout)
		self.vLayout.addWidget(self.vTimePointScrollArea)

		self.vLayout.addLayout(self.hLayout2)
		
		self.timer = QTimer(self)
		self.timer.timeout.connect(self.nextImage)
		self.timer.start(self.speedToTime(animationSpeed))

		self.refreshTimer = QTimer(self)
		self.refreshTimer.setSingleShot(True)
		self.refreshTimer.timeout.connect(self.openImages)
		
		self.artifactTypes = ["None", "Motion", "FoV", "Noise", "Other"]

		self.setFocusPolicy(Qt.StrongFocus)

		self.setLayout(self.vLayout)
		self.openPatient()
		return
	
	def createTimePointLayout(self, patient):
		self.emptyLayout(self.vTimePointLayout)
		self.checkboxes = {}
		self.artifactTypeComboBoxes = {}
		self.commentLineEdits = {}
		n = 0
		for datasetId, row in self.current_patient_datasets_to_control().iterrows():
		# for time in patient.ls():
			n += 1
			time = row.acquisition_date
			self.createArtifactTypeLayout(time, datasetId)
			self.createCommentLayout(time, datasetId)
			self.createCheckboxLayout(time, datasetId, n)
		return
	
	def createCheckboxLayout(self, time, datasetId, imageNumber):
		checkbox = QtWidgets.QCheckBox(f"{imageNumber}. Temporal point {time} {datasetId} valid")
		checkbox.setCheckState(Qt.Checked)
		checkbox.stateChanged.connect(partial(self.checkboxChanged, timePoint=time, datasetId=datasetId))
		self.checkboxes[datasetId] = (imageNumber, checkbox)
		self.vTimePointLayout.addWidget(checkbox)
		return

	def createCommentLayout(self, time, datasetId):
		commentLayout = QtWidgets.QHBoxLayout()
		commentLabel = QtWidgets.QLabel(self)
		commentLabel.setText('Comment:')
		commentLineEdit = QtWidgets.QLineEdit(self)
		commentLineEdit.setText('')
		commentLineEdit.editingFinished.connect(partial(self.comment, timePoint=time, datasetId=datasetId))
		commentLayout.addWidget(commentLabel)
		commentLayout.addWidget(commentLineEdit)
		self.commentLineEdits[datasetId] = commentLineEdit
		self.vTimePointLayout.addLayout(commentLayout)
		return
	
	def createArtifactTypeLayout(self, time, datasetId):
		artifactTypeLayout = QtWidgets.QHBoxLayout()
		artifactTypeLabel = QtWidgets.QLabel(self)
		artifactTypeLabel.setText('Artifact type:')
		artifactTypeComboBox = QtWidgets.QComboBox()
		artifactTypeComboBox.addItems(self.artifactTypes)
		artifactTypeLayout.addWidget(artifactTypeLabel)
		artifactTypeLayout.addWidget(artifactTypeComboBox)
		self.artifactTypeComboBoxes[datasetId] = artifactTypeComboBox
		self.vTimePointLayout.addLayout(artifactTypeLayout)
		artifactTypeComboBox.currentIndexChanged.connect(partial(self.artifactTypeChange, timePoint=time, datasetId=datasetId))
		return

	# def onPatientDirChange(self, patientDir):
	# 	self.dataDirectory = Path(patientDir)
	# 	self.onPreviewDirChange(patientDir)
	# 	return
		
	# def onPreviewDirChange(self, patientDir):
	# 	self.previewDirectory = Path(patientDir)
	# 	self.patients = self.previewDirectory.ls()
	# 	self.patients = [patient for patient in self.patients if patient.is_dir()]
	# 	self.sortPatients()
	# 	self.populateImageWidgetLayout()
	# 	self.currentPatient = 0
	# 	self.openPatient()
	# 	self.previewDirLineEdit.setText(str(patientDir))
	# 	return

	def openPathAndSetLineEdit(self, path, lineEdit, onFileChange):
		lineEdit.setText(path)
		onFileChange(path)
		return
	
	def createFileUI(self, name, onFileChange, fileType='directory', save=False):
		layout = QtWidgets.QHBoxLayout()
		label = QtWidgets.QLabel(self)
		label.setText(name + ':')
		lineEdit = QtWidgets.QLineEdit(self)
		lineEdit.setText('')
		lineEdit.editingFinished.connect(lambda: onFileChange(lineEdit.text()))
		fileButton = QtWidgets.QPushButton("...")
		if fileType == "directory":
			fileButton.clicked.connect(lambda: self.openPathAndSetLineEdit(QtWidgets.QFileDialog.getExistingDirectory(self, self.tr("Choose " + name), "~/"), lineEdit, onFileChange))
		else:
			if save:
				fileButton.clicked.connect(lambda: self.openPathAndSetLineEdit(QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Choose " + name), "~/", fileType), lineEdit, onFileChange))
			else:
				fileButton.clicked.connect(lambda: self.openPathAndSetLineEdit(QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Choose " + name), "~/", fileType), lineEdit, onFileChange))

		layout.addWidget(label)
		layout.addWidget(lineEdit)
		layout.addWidget(fileButton)
		return layout, label, lineEdit, fileButton

	def speedToTime(self, speed):
		return 1000 / pow(speed, 2)
	
	def keyPressEvent(self, event):
		# if QApplication.focusWidget() == self.patientLineEdit or QApplication.focusWidget() == self.commentLineEdit:
		# 	return
		if event.key() == QtCore.Qt.Key_Escape and self.imageWidget:
			self.imageWidget.setFocus()
			return
		
		focusWidget = QtWidgets.QApplication.focusWidget()
		if isinstance(focusWidget, QtWidgets.QLineEdit) or isinstance(focusWidget, QtWidgets.QSpinBox) or isinstance(focusWidget, QtWidgets.QTextEdit) or isinstance(focusWidget, QtWidgets.QDateTimeEdit) or isinstance(focusWidget, QtWidgets.QPlainTextEdit):
			return
		
		modifiers = QtWidgets.QApplication.keyboardModifiers()
		if event.key() == QtCore.Qt.Key_Left:
			if modifiers == QtCore.Qt.ShiftModifier:
				self.previousUnseenPatient()
			else:
				self.previousPatient()
		elif event.key() == QtCore.Qt.Key_Right:
			if modifiers == QtCore.Qt.ShiftModifier:
				self.nextUnseenPatient()
			else:
				self.nextPatient()
		elif event.key() == QtCore.Qt.Key_Up:
			self.previousImage()
		elif event.key() == QtCore.Qt.Key_Down:
			self.nextImage()
		elif event.key() == QtCore.Qt.Key_C:
			for datasetId in self.commentLineEdits.keys():
				self.commentLineEdits[datasetId].setFocus(Qt.FocusReason.ShortcutFocusReason)
				break

		number_keys = [QtCore.Qt.Key_1, QtCore.Qt.Key_2, QtCore.Qt.Key_3, QtCore.Qt.Key_4, QtCore.Qt.Key_5, QtCore.Qt.Key_6, QtCore.Qt.Key_7, QtCore.Qt.Key_8, QtCore.Qt.Key_9]
		for i, key in enumerate(number_keys):
			if event.key() == key and len(self.checkboxes) > i:
				checkbox = [cb[1] for index, cb in self.checkboxes.items() if cb[0] == (i+1)][0]
				checkbox.setCheckState(Qt.Checked if checkbox.checkState() != Qt.Checked else Qt.Unchecked)

		return
	
	def viewModeChange(self, i):
		self.populateImageWidgetLayout()
		self.refreshTimer.start(100)
		self.openImages()
		return
	
	# def setSequences(self, patient, timePoint, key, value):
		# self.datasets.loc[(self.datasets.id_hd == patient) & (self.datasets.acquisition_date == timePoint), key] = value
	def setSequences(self, index, key, value):
		self.datasets.loc[index, key] = value
		if self.datasets_path.endswith('.xlsx') or self.datasets_path.endswith('.xls'):
			self.datasets.to_excel(self.datasets_path)
		else:
			self.datasets.to_csv(self.datasets_path)
		return

	def artifactTypeChange(self, i, timePoint, datasetId):
		artifactType = self.artifactTypeComboBoxes[datasetId].itemText(i)
		self.setSequences(datasetId, 'artifactType', artifactType)
		return
	
	def comment(self, timePoint, datasetId):
		comment = self.commentLineEdits[datasetId].text()
		self.setSequences(datasetId, 'comment', comment)
		return
	
	def checkboxChanged(self, value, timePoint, datasetId):
		if value == Qt.Unchecked or value == Qt.Checked:
			valid = True if value == Qt.Checked else False
			self.setSequences(datasetId, 'valid', valid)
		return

	def setCurrentPatient(self):
		patientName = self.patientLineEdit.text()
		for patient in self.patients:
			if patientName in patient:
				self.currentPatient = patient
				self.openPatient()
				break
		return

	def extractSliceInfo(self, name):
		regex = r"(center_)?(?P<planeName>axial|sagittal|coronal)_(?P<index>\d+)\.png"
		match = re.match(regex, name)
		if match is None: return None, None
		return match.group('planeName'), int(match.group('index'))
	
	def getPreviewImages(self, time, datasetId):
		previewDir = self.previewDirectory / f'{self.currentPatient}_{time}' / f'preview_{datasetId}'
		if self.isMosaic():
			name = 'mosaicH.png' if 'Horizontal' in self.viewMode() else 'mosaicV.png'
			path = previewDir / name
			if not path.exists():
				print('Mosaic image not found in', path.parent)
			return [{ 'mosaic': path}]
		imagePaths = sorted(list(previewDir.iterdir())) if previewDir.is_dir() else None
		if not imagePaths or len(imagePaths) == 0:
			print('No images found in', previewDir)
			return None
		imagePathsDict = {}
		for imagePath in imagePaths:
			if 'center' in imagePath.name: continue
			planeName, index = self.extractSliceInfo(imagePath.name)
			if planeName is None or index is None: continue
			if index not in imagePathsDict: imagePathsDict[index] = {}
			imagePathsDict[index][planeName] = imagePath
		# return collections.OrderedDict(sorted(imagePathsDict.items()))
		return [value for key, value in sorted(imagePathsDict.items())]
	
	def createPixmapFromImageIndex(self, imageLabel, imageIndex, planeName, imagePaths):
		imageIndex = imageIndex if not self.isMosaic() else 0
		imageExists = imagePaths is not None and imageIndex >= 0 and imageIndex < len(imagePaths) and planeName in imagePaths[imageIndex]
		imagePath = str(imagePaths[imageIndex][planeName]) if imageExists else ''
		if not imageExists:
			# print('image does not exist:')
			# print('patient', self.currentPatient, 'image', self.currentImage, planeName)
			# print(imagePaths)
			return
		pixmap = QtGui.QPixmap(imagePath).scaled(imageLabel.width(), imageLabel.height(), Qt.KeepAspectRatio)
		# pixmap.setWidgetResizable(True)
		imageLabel.setPixmap(pixmap)
		return

	def createImageLabel(self, imageLayout, time, datasetId, imageNumber, planeName):
		imageLabel = QtWidgets.QLabel(self)
		self.createPixmapFromImageIndex(imageLabel, self.currentImage, planeName, self.imagePaths[datasetId])
		imageLabel.show()
		self.imageLabels[datasetId].append(imageLabel)

		layout = QtWidgets.QVBoxLayout()
		imageIndexLabel = QtWidgets.QLabel(self)
		imageIndexLabel.setText(f'{imageNumber}. {time} {datasetId}')
		imageIndexLabel.setFont(QFont('Times', 6))
		layout.addWidget(imageLabel, 1000)
		layout.addWidget(imageIndexLabel, 1)
		imageLayout.addLayout(layout)

		# imageLayout.addWidget(imageLabel)
		return
	
	def createAllImagesLayout(self, time, datasetId, imageNumber):
		imageLayout = QtWidgets.QHBoxLayout()
		if (self.datasets.id_hd == self.currentPatient).sum() == 0:
			print('Error: patient does not exist ', self.currentPatient)
			return imageLayout
		self.imagePaths[datasetId] = self.getPreviewImages(time, datasetId)
		self.imageLabels[datasetId] = []
		if self.imagePaths[datasetId] is None: return imageLayout
		for planeName in planeNames:
			self.createImageLabel(imageLayout, time, datasetId, imageNumber, planeName)
		return imageLayout

	def createAxialImageLayout(self):
		imageLayout = QtWidgets.QHBoxLayout()
		if (self.datasets.id_hd == self.currentPatient).sum() == 0:
			print('Error: patient does not exist ', self.currentPatient)
			return imageLayout
		n = 0
		for index, row in self.current_patient_datasets_to_control().iterrows():
			n += 1
			time = row.acquisition_date
			self.imagePaths[index] = self.getPreviewImages(time, index)
			self.imageLabels[index] = []
			if self.imagePaths[index] is None:
				del self.imagePaths[index]
				continue
			self.createImageLabel(imageLayout, time, index, n, 'axial')
		return imageLayout

	def createMosaicImageLayout(self):
		# imageLayout = QtWidgets.QVBoxLayout() if 'Horizontal' in self.viewMode() else QtWidgets.QHBoxLayout()
		imageLayout = QtWidgets.QHBoxLayout()
		if (self.datasets.id_hd == self.currentPatient).sum() == 0:
			print('Error: patient does not exist ', self.currentPatient)
			return imageLayout
		n = 0
		for index, row in self.current_patient_datasets_to_control().iterrows():
			n += 1
			time = row.acquisition_date
			self.imagePaths[index] = self.getPreviewImages(time, index)
			self.imageLabels[index] = []
			if self.imagePaths[index] is None: return imageLayout
			self.createImageLabel(imageLayout, time, index, n, 'mosaic')
		return imageLayout
	
	def emptyLayout(self, layoutOrItem, deleteIt=False):
		if isinstance(layoutOrItem, QtWidgets.QWidgetItem):
			if deleteIt:
				layoutOrItem.widget().deleteLater()
		else:
			while layoutOrItem.count() > 0:
				child = layoutOrItem.takeAt(0)
				self.emptyLayout(child, True)
			if deleteIt:
				del layoutOrItem
		return

	def populateImageWidgetLayout(self):
		self.imageLabels = {}
		if self.imageWidgetLayout:
			self.emptyLayout(self.imageWidgetLayout)
			
			if self.viewModes[self.viewModeComboBox.currentIndex()] == "Axial":
				self.imageWidgetLayout.addLayout(self.createAxialImageLayout())
			elif 'mosaic' in self.viewModes[self.viewModeComboBox.currentIndex()]:
				self.imageWidgetLayout.addLayout(self.createMosaicImageLayout())
			else:
				n = 0
				for index, row in self.current_patient_datasets_to_control().iterrows():
					n += 1
					time = row.acquisition_date
					self.imageWidgetLayout.addLayout(self.createAllImagesLayout(time, index, n))

		return
	
	# def imageWidgetResized(self, event):
	# 	self.repaint()
	# 	self.resizePixmaps()
	# 	return

	def createImageWidget(self):
		# self.scrollArea = QtWidgets.QScrollArea(self)
		# self.imageWidget.setWidgetResizable(True)
		self.imageWidget = QtWidgets.QWidget(self)

		self.imageWidget.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
		self.imageWidget.setMinimumSize(400,400)

		self.imageWidgetLayout = QtWidgets.QVBoxLayout()
		self.populateImageWidgetLayout()
		self.imageWidget.setLayout(self.imageWidgetLayout)
		self.imageWidget.setFocusPolicy(Qt.StrongFocus)
		self.imageWidget.setFocus()
		
		# self.imageWidget.resized.connect(self.imageWidgetResized)
		# self.imageWidget.resizeEvent = (lambda old_method: (lambda event: (self.imageWidgetResized(event), old_method(event))[-1]))(self.imageWidget.resizeEvent)

		# self.scrollArea.setWidget(self.imageWidget)
		# return self.scrollArea
		return self.imageWidget
	
	def viewMode(self):
		return self.viewModes[self.viewModeComboBox.currentIndex()]
	
	def isMosaic(self):
		return "mosaic" in self.viewMode()

	def current_patient_datasets_to_control(self):
		return self.datasets[(self.datasets.id_hd == self.currentPatient) & (self.datasets.ready_for_quality_control == True)]

	def openImages(self):
		for index, row in self.current_patient_datasets_to_control().iterrows():
			time = row.acquisition_date
			if index not in self.imageLabels: continue
			for i, imageLabel in enumerate(self.imageLabels[index]):
				planeName = 'mosaic' if self.isMosaic() else planeNames[i]
				self.createPixmapFromImageIndex(imageLabel, self.currentImage, planeName, self.imagePaths[index])
		self.repaint()
		return

	def openPatient(self):
		self.currentImage = 0
		self.createTimePointLayout(self.currentPatient)
		datasets_to_control = self.current_patient_datasets_to_control()
		for index, row in datasets_to_control.iterrows():
			time = row.acquisition_date
			artifactType = row['artifactType'] if 'artifactType' in row and type(row['artifactType']) is str else ''
			checkState = (Qt.Checked if row['valid'] else Qt.Unchecked) if 'valid' in row and type(row['valid']) is bool else Qt.PartiallyChecked
			comment = row['comment'] if 'comment' in row and type(row['comment']) is str else ''
			self.artifactTypeComboBoxes[index].setCurrentIndex(self.artifactTypes.index(artifactType) if artifactType in self.artifactTypes else -1)
			self.checkboxes[index][1].setCheckState(checkState)
			self.commentLineEdits[index].setText(comment)
		self.patientLineEdit.setText(self.currentPatient)
		if self.timer.isActive():
			self.timer.start()
		self.refreshTimer.start(100)
		for index, row in datasets_to_control.iterrows():
			time = row.acquisition_date
			self.imagePaths[index] = self.getPreviewImages(time, index)
		self.populateImageWidgetLayout()
		self.openImages()
		return

	def previousPatient(self):
		self.currentPatient = self.patients[max(0, self.patients.index(self.currentPatient) - 1)]
		self.openPatient()
		return

	def nextPatient(self):
		self.currentPatient = self.patients[min(len(self.patients)-1, self.patients.index(self.currentPatient) + 1)]
		self.openPatient()
		return

	def patientWasSeen(self):
		return self.current_patient_datasets_to_control().valid.isna().sum() == 0
	
	def previousUnseenPatient(self):
		while self.currentPatient != self.patients[0] and self.patientWasSeen():
			self.currentPatient = self.patients[max(0, self.patients.index(self.currentPatient) - 1)]
		self.openPatient()
		return
	
	def nextUnseenPatient(self):
		while self.currentPatient < self.patients[-1] and self.patientWasSeen():
			self.currentPatient = self.patients[min(len(self.patients)-1, self.patients.index(self.currentPatient) + 1)]
		self.openPatient()
		return
	
	def previousImage(self):
		if self.isMosaic(): return
		nImages = [len(self.imagePaths[ip]) for ip in self.imagePaths if self.imagePaths[ip] is not None]
		if len(nImages) == 0: return
		nImages = np.max(nImages)
		# nImages = np.max([len(list(time.glob('**/*.png')))for time in self.patients[self.currentPatient].ls()])
		# nImages = max(len(self.imagePaths['time01']), len(self.imagePaths['time02'])) if self.imagePaths['time01'] and self.imagePaths['time02'] else 1
		self.currentImage -= 1
		if self.currentImage < 0:
			self.currentImage = nImages - 1
		self.openImages()
		return
	
	def nextImage(self):
		if self.isMosaic(): return
		nImages = [len(self.imagePaths[ip]) for ip in self.imagePaths if self.imagePaths[ip] is not None]
		if len(nImages) == 0: return
		nImages = np.max(nImages)
		# nImages = np.max([len(list(time.glob('**/*.png')))for time in self.patients[self.currentPatient].ls()])
		# nImages = max(len(self.imagePaths['time01']), len(self.imagePaths['time02'])) if self.imagePaths['time01'] and self.imagePaths['time02'] else 1
		self.currentImage += 1
		if self.currentImage >= nImages:
			self.currentImage = 0
		self.openImages()
		return
	
	def openImageInFSLEyes(self):
		# patient = self.patients[self.currentPatient]
		folders = [self.previewDirectory / f'{self.currentPatient}_{r.acquisition_date}' for i, r in self.current_patient_datasets_to_control().iterrows()]
		flairs = set([flair for folder in folders for flair in list(folder.glob('*.nii.gz'))])

		process = QtCore.QProcess(self)
		# process.start('fsleyes', ['--cliserver', '--runscript', '/home/amasson/ms_study/preparations/closeFsleyes.py'])
		# process.finished.connect(self.processFinished)

		self.command = [str(flair) for flair in flairs]
		# self.command = ['--cliserver', '-vl', str(center[2]), str(center[1]), str(center[0]), str(time0), str(time1), str(segmentation), '-cm', 'Red', '-a', '50']
		process.start('fsleyes', self.command)

		# process.finished.connect(self.processFinished)
		return
	
	def startTimer(self):
		animationSpeed = 0
		try:
			animationSpeed = int(self.speedSpinBox.value())
		except:
			return
		if animationSpeed == 0:
			self.timer.stop()
		else:
			self.timer.start(self.speedToTime(animationSpeed))
		return
	
	def speedChanged(self):
		self.startTimer()
		return

	def resizePixmaps(self):
		for index in self.imageLabels:
			for imageLabel in self.imageLabels[index]:
				imageLabel.setPixmap(imageLabel.pixmap().scaled(imageLabel.width(), imageLabel.height(), Qt.KeepAspectRatio))
		return
	
	def resizeEvent(self, event):
		self.resizePixmaps()
		return

	# def showTime(self):
	#     time = QDateTime.currentDateTime()
	#     timeDisplay = time.toString('yyyy-MM-dd hh:mm:ss dddd')
	#     self.label.setText(timeDisplay)
	
if __name__ == "__main__":
	app = QtWidgets.QApplication([])


	gui_widget = QCGUI(datasets, args.data, args.preview, args.datasets)
	
	widget = QtWidgets.QWidget()
	widget.setWindowTitle("QC Tool")
	widget.resize(1000, 750)

	area = QtWidgets.QScrollArea()
	area.setWidget(gui_widget)
	vlayout = QtWidgets.QVBoxLayout()
	vlayout.addWidget(gui_widget)
	widget.setLayout(vlayout)
	
	vlayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)

	widget.show()

	sys.exit(app.exec_())